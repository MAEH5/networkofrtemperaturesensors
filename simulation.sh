#!/bin/bash
if [ $# -ne 5 ]
then
    echo "Incorrect number of arguments"
    exit -1
fi
topic=$1
minT=$2
maxT=$3
minH=$4
maxH=$5
while true
do
    randT=$(shuf -i $minT-$maxT -n 1)
    randH=$(shuf -i $minH-$maxH -n 1)
    mosquitto_pub -m $(printf '{"temperature":%d,"humidity":%d}' "$randT" "$randH") -t $topic
    sleep 1
done