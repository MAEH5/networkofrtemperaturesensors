import paho.mqtt.client as mqtt
from flask import Flask, render_template, request ,jsonify
import json, sqlite3, time, threading

mqttc=mqtt.Client()
brokerIp="0.0.0.0"
WAIT_SECONDS = 1
MINTEMP=0
MAXTEMP=40
topices=["/esp8266/1/dhtreadings","/esp8266/2/dhtreadings","/esp8266/3/dhtreadings"]
lastTemperatures=list(range(0,len(topices)))

#create a separate thread that publish a message based on the temperature
def alert():
    myMean=mean(lastTemperatures)
    mqttc.publish("/mean",myMean)
    if   myMean > MAXTEMP : mqttc.publish("/led/1","1")
    elif myMean < MINTEMP : mqttc.publish("/led/3","1")
    else : mqttc.publish("/led/2","1")
    threading.Timer(WAIT_SECONDS, alert).start()

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def mean(table):
    result=0
    for i in table:
        result+=i/len(table)
    return result

#subscribe to the differente topices presente in the global variable topices
def on_connect(client, userdata, flags, rc):
    for topice in topices:
        client.subscribe(topice)
        
#insert temperature and humidity values in the DB
def on_message(client, userdata, message):
    topic=message.topic
    if topic in topices :
        conn=sqlite3.connect('sensordata.db')
        c=conn.cursor()
        dhtreadings_json = json.loads(message.payload.decode("utf-8"))
        device=topic.split('/')[1]+"/"+topic.split('/')[2]
        lastTemperatures[int(device.split('/')[-1])-1]=dhtreadings_json['temperature']
        c.execute("""INSERT INTO dhtreadings (temperature,humidity, currentdate, currentime, device) 
                VALUES((?), (?), date('now'),time('now'), (?))""", (dhtreadings_json['temperature'],
                dhtreadings_json['humidity'], device) )
        conn.commit()
        conn.close()

#return a json of the last 10 mesurements of temperature
def dataTemperature(device):
    conn=sqlite3.connect('sensordata.db')
    c=conn.cursor()
    temperature=[]
    conn.row_factory = dict_factory
    c.execute("""SELECT temperature FROM dhtreadings where device=(?) ORDER BY id DESC LIMIT 10""",(device,))
    readings = c.fetchall()
    for row in readings:
        temperature.append(row[0])
    conn.close()
    return jsonify(temperature)

def preparations() :
    mqttc.on_connect = on_connect
    mqttc.on_message = on_message
    mqttc.connect(brokerIp,1883,60)
    mqttc.loop_start()
    alert()

app = Flask(__name__)
@app.route('/')
def main():
    return render_template('main.html')

@app.route('/data/<deviceNum>',methods=['GET', 'POST'])
def dataTemp(deviceNum):
    return dataTemperature("esp8266/"+str(deviceNum))

@app.route('/mean/',methods=['GET', 'POST'])
def dataMean():
    return jsonify(myMean=mean(lastTemperatures))

if __name__ == "__main__":
    preparations()
    app.run(host='0.0.0.0', port=8181, debug=True)